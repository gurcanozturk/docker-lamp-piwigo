FROM alpine:latest

RUN apk update && \
	apk --no-cache add \
	php7 \
	php7-bz2 \
	php7-common \
	php7-curl \
	php7-gd \
	php7-fpm \
	php7-json \
	php7-mbstring \
	php7-session \
	php7-mysqli \
	php7-opcache \
	php7-cgi \
	php7-zip \
	php7-xmlrpc \
	php7-tidy \
	php7-apache2 \
	apache2 \
	curl \
	pwgen \
    mariadb-common \
    mariadb \
    mariadb-client \
    mariadb-server-utils
    
RUN rm /var/www/localhost/htdocs/index.html && \
    curl http://piwigo.org/download/dlcounter.php?code=latest -o /tmp/piwigo.zip && \
    unzip -qq /tmp/piwigo.zip -d /var/www/localhost/htdocs/ && \
    mv /var/www/localhost/htdocs/piwigo/* /var/www/localhost/htdocs/ && \
    rm -rf /tmp/piwigo.zip && \
    rm -rf /var/cache/apk/*

ADD run.sh /scripts/run.sh
RUN chmod +x /scripts/run.sh

EXPOSE 80 443

VOLUME ["/var/lib/mysql", "/var/www/localhost/htdocs/galleries/"]

ENTRYPOINT ["/scripts/run.sh"]