Alpine Linux based Apache/PHP/MariaDB docker image which runs Piwigo Online Gallery.

Build it.
build -t docker-lamp-piwigo .

Run it. 

docker run -it --rm -p 8008:80 \
    -e MYSQL_DATABASE=MYSQL_DB \
    -e MYSQL_USER=DB_USER \
    -e MYSQL_PASSWORD=<DB_USERL_PASSWORD> \
    -e MYSQL_ROOT_PASSWORD=<MYSQL_ROOT_PASSWORD> \
    -v /opt/docker/volumes/piwigo-db:/var/lib/mysql/piwigo \
    -v /opt/docker/volumes/piwigo-gallery:/var/www/localhost/htdocs/galleries docker-lamp-piwigo
